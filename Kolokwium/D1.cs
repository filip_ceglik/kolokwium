using System;

namespace Kolokwium
{
    public class D1
    {
        public double fun(double x, double y)
        {
            return Math.Pow(x,2)*0.5-10*x+1-y;
        }
        
        public void zad1d()
        {
            string x = "a";
            string y = "a";
            while (true)
            {
                Console.WriteLine("Podaj x");
                x = Console.ReadLine();
                Console.WriteLine("Podaj y");
                y = Console.ReadLine();
                if (fun(double.Parse(x), Double.Parse(y)) == 0)
                {
                    Console.WriteLine("Należy");
                }
                else
                {
                    Console.WriteLine("Nie należy");
                }
            }
        }
        
        
    }
}